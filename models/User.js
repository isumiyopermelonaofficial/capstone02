// [SECTION] Dependencies and Modules
const mongoose = require("mongoose");

// [SECTION] Blueprint Schema
const userSchema = new mongoose.Schema({
	email: {
		type: String,
		required: [true, 'Email is Required']
	},

	password: {
		type: String,
		required: [true, 'Password is Required']
	},

	isAdmin: {
		type: Boolean,
		default: false
	},

	orders: [
	{
			orderId: {
			type: String,
			required: [true, 'Order ID is Required']
		},

		purchasedOn: {
			type: Date,
			required: new Date()
		}
	}

	]
});


// [SECTION] Model
const User = mongoose.model('User', userSchema);
module.exports = User;