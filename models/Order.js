// [SECTION] Dependencies and Modules
const mongoose = require('mongoose');

// [SECTION] Blueprint Schema
const orderSchema = new mongoose.Schema({
	totalAmount: {
		type: Number,
		required: [true, 'Total Amount is Required']
	},

	purchasedOn: {
		type: Date,
		default: new Date()
	},

	orderedBy: [
		{
			userId: {
				type: String,
				required: [true, 'User ID is Required']
			},

			email: {
				type: String,
				required: [true, 'User Email is Required']
			}
		}
	],

	productList: [
		{
			productId: {
				type: String,
				required: [true, 'Product ID is Required']
			},

			name: {
				type: String,
				required: [true, 'Name is Required']
			},

			quantity: {
				type: Number,
				required: [true, 'Quantity is Required']
			},

			description: {
				type: String,
				required: [true, 'Description is Required']
			},

			price: {
				type: Number,
				required: [true, 'Price is Required']
			}
		}

	]
});

// [SECTION] Model
const Order = mongoose.model('Order', orderSchema);
module.exports = Order;