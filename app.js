// [SECTION] Packages and Dependencies
const express = require("express");
const mongoose = require("mongoose");
const dotenv = require("dotenv");

const userRoutes = require('./routes/users.js');
const productRoutes = require('./routes/products.js');
const orderRoutes = require('./routes/orders.js');

// [SECTION] Server
const app = express();
dotenv.config();

app.use(express.json());

const port = process.env.PORT;
const connString = process.env.CONNECTION_STRING;

// [SECTION] Application Routes
app.use('/users', userRoutes);
app.use('/products', productRoutes);
app.use('/orders', orderRoutes);


// [SECTION] Database Connection
mongoose.connect(connString);

let connectionStatus = mongoose.connection;
connectionStatus.on('open', () => console.log(`Database is Connected`));

app.get('/', (req,res) => {
	res.send("Welcome to Isumiyo's - Ecommerce API");
});


// [SECTION] Gateway Response
app.listen(port, () => console.log(`Server is running on port ${port}`));