//[SECTION] Dependencies and Modules
const Product = require('../models/Product.js');
const exp = require('express');
const auth = require('../auth.js');
const { findById } = require('../models/Product.js');

//[SECTION] CREATE

// Create a Product - ADMIN ONLY
module.exports.createProduct = (req,res) => {
	let newName = req.body.name;
	let newDesc = req.body.description;
	let newPrice = req.body.price;

	let newProduct = new Product({
		name: newName,
		description: newDesc,
		price: newPrice
	});

	return Product.findOne({name:newName}).then(foundProduct => {
		if (foundProduct) {
			res.send({Message:`Product ${newName} is already Registered`});
		} else {
			return newProduct.save().then((success,error) => {
				if (error) {
					res.send({Message:"Error Unable to Save Product"});
				} else {
					res.send(success);
				}
			});
		};
	});	
};

//[SECTION] RETRIEVE

// Get All Products
module.exports.getAllProducts = (req,res) => {
	return Product.find({}).then(result => {
		res.send(result);
	});
};

//Get a Single Product
module.exports.getProduct = (req,res) => {
	let id = req.params.id;
	return Product.findById(id).then((success,error) => {
		if (error) {
			res.send({Message:"Error: Product ID not Found"});
		} else {
			res.send(success);
		}
	});
};

//Get All Active Products
module.exports.getAllActive = (req,res) => {
	return Product.find({isActive:true}).then(result => {
		res.send(result);
	});
};

//[SECTION] UPDATE

// Update the Product - ADMIN ONLY
module.exports.updateProduct = (req,res) => {
	let id = req.params.id;
	let upName = req.body.name;
	let upDesc = req.body.description;
	let upPrice = req.body.price

	if (upName !== '' && upDesc !== '' && upPrice !== '') {
		let updatedProduct = {
			name:upName,
			description:upDesc,
			price:upPrice
		};
		return Product.findByIdAndUpdate(id,updatedProduct).then((success,error) => {
			if (error) {
				res.send({Message:"Error: Unable to Update Product Information"});
			} else {
				res.send({Message:"Product Information Successfully Updated"});
			}
		});
	} else {
		res.send({Message:"Error: All Inputs are Required"});
	}
};

module.exports.deactivateProduct = (req,res) => {
	let id = req.params.id;
	let updates = {
		isActive:false
	};
	return Product.findByIdAndUpdate(id,updates).then((success,error) => {
		if (error) {
			res.send({Message:"Error: Unable to Archive Product"});
		} else {
			res.send({Message:"Product Successfully Moved to Archive"});
		}
	});
};

//[SECTION] DELETE
module.exports.deleteProduct = (req,res) => {
	let id = req.params.id;
	return Product.findByIdAndRemove(id).then((removedProduct,error) => {
		if (error) {
			res.send({Message:"Error: No Product was Removed"});
		} else {
			res.send({Message:"Product Successfully Removed"});
		}
	});
};
