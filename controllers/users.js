// [SECTION] Dependencies and Modules
const User = require('../models/User.js');
const bcrypt = require('bcrypt');
const dotenv = require('dotenv');
const auth = require('../auth.js');
const jwt = require('jsonwebtoken');
const { findOne } = require('../models/User.js');
const res = require('express/lib/response');

dotenv.config();
const salt = Number(process.env.SALT);

// [SECTION] CREATE

// Create New User
module.exports.registerUser = (req,res) => {
	let userEmail = req.body.email;
	let userPassword = req.body.password;

	let newUser = new User({
		email:userEmail,
		password:bcrypt.hashSync(userPassword,salt)
	});
		return User.findOne({email:userEmail}).then(foundUser => {
		if(foundUser){
			 res.send({message: "User Already Registered"});
		} else {
			return newUser.save().then((user,err) => {
				if (err){
					res.send({message: "Error: Unable to Register User"});
				} else {
					//return `User Successfully Registered ${user}`;
					res.send({message: `User ${userEmail} Succesfully Registered`});
				}
			});
		}
	});
};

//User Login
module.exports.loginUser = (req,res) => {
	let userEmail = req.body.email;
	let userPass = req.body.password;

	return User.findOne({email:userEmail}).then(foundUser => {
	if (foundUser === null) {
		res.send({message:"Wrong Credentials"});
	} else {
		const isPasswordCorrect = bcrypt.compareSync(userPass, foundUser.password);
		if(isPasswordCorrect){
			res.send({LoginToken:auth.createAccessToken(foundUser)});
		} else {
			res.send({message:"Error: Incorrect Password"});
		}
	}
	});
};

// [SECTION] Retrieve

//Get All Users
module.exports.getAllUsers = (req,res) => {
	return User.find({}).then(result => {
		res.send(result);
	});
};

// Get Single User
module.exports.getUser = (req,res) => {
	return User.findById(req.params.id).then(result => {
		res.send(result);
	});
};

// [SECTION] Update

module.exports.setAdmin = (req,res) => {
	let id = req.params.id;
	let updateUser = {
		isAdmin:true
	};
	return User.findByIdAndUpdate(id,updateUser).then((success,error) => {
		if (error) {
			res.send({Message:"Error: Unable to Set User as Admin"});
		} else {
			res.send({Message:"User Successfully set User as Admin"});
		}
	});
};

// [SECTION] Delete
module.exports.deleteUser = (req,res) => {
	let id = req.params.id;
	return User.findByIdAndRemove(id).then((success,error) => {
		if (error) {
			res.send({Message:"Error: Unable to Delete User"});
		} else {
			res.send({Message:"User Successfully Deleted"});
		}
	});
};