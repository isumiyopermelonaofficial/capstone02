const Order = require('../models/Order.js');
const Product = require('../models/Product.js');
const User = require('../models/User.js');
const auth = require("../auth.js")

//[SECTION] CREATE

// Create New Order
module.exports.createOrder = async(req,res) => {
	let newTotal = 0;
	let newUserId = req.user.id;
	let newUserEmail = req.user.email;

	let newOrder = new Order({
		totalAmount:newTotal,
		orderedBy:[
		{
			userId:newUserId,
			email:newUserEmail
		}	
	]
	});

	let createNewOrder = await User.findById(req.user.id).then(foundUser => {
		if (foundUser) {
			return newOrder.save().then((ordercreated,error) => {
				res.send(ordercreated);

				let update = {
					orderId:ordercreated.id
				};

				return User.findByIdAndUpdate(foundUser.id,update).then((success,error) => {
					if (success) {
						return res.send({Message:ordercreated});
					} else {
						res.send({Message:"Error"});
					}
				});
				//res.send({Message:ordercreated});
			});
		} else {
			res.send({Message:"User not Registered"});
		};
	});

	//let updateUserOrder = await 
	// let newOrdered =  await Order.save().then((ordercreated,error) => {
	// 	if (error) {
	// 		res.send({Message:"Unable to Create Order"});
	// 	} else {
	// 		let userUpdate = {
	// 			orderId:ordercreated.id
	// 		};
	// 		ordercreated.orders.push(userUpdate);
	// 		return ordercreated.save().then(ordercreated => true).catch(err => err.message);
	// 		//console.log(ordercreated.id);
	// 		Users.orders.push(userUpdate)
	// 		return User.findByIdAndUpdate(req.user.id,userUpdate).then(success => true);
	// 		res.send({Message:ordercreated});
	// 	}
	// });
};

// Add to Order
module.exports.addToOrder = async (req,res) => {
	let orderId = req.body.orderId;
	let userId = req.user.id;
	let productId = req.body.productId;
	let orderQuantity = req.body.quantity;

	let checkOrder = await Order.findById(orderId).then(foundOrder => {
		if (foundOrder) {
			let currentTotal = foundOrder.totalAmount;

			return Product.findById(productId).then(foundProduct => {
				let prodID = foundProduct._id;
				let prodName = foundProduct.name;
				let prodDesc = foundProduct.description;
				let prodPrice = foundProduct.price;
				let newTotal = prodPrice * orderQuantity;
				let newTotalAmount = newTotal + currentTotal;

				let updateOrder = {
					totalAmount:newTotalAmount
				};
				Order.findByIdAndUpdate(orderId,updateOrder).then(success => true);

				let newProduct = {
					productId:prodID,
					name:prodName,
					quantity:orderQuantity,
					description:prodDesc,
					price:prodPrice,
				};
				foundOrder.productList.push(newProduct);
				res.send({Message:"Added to Cart"});
				return foundOrder.save().then(foundOrder => true).catch(err => err.message);		
			});
		} 
		else {
			res.send({Message:"Order NOT FOUND"});
		}
	});

	if(checkOrder !== true){
		return res.send({Message: checkOrder});
	}
};

//[SECTION] RETRIEVE
//retrieve Auth User Order
module.exports.getAllOrders = (req,res) => {
	return Order.find({}).then(result => {
		res.send(result);
	});
};


module.exports.getUserOrder = async(req,res) => {
	let orderId = req.params.id;
	//console.log(orderId);
	let getOrders = await Order.findById(orderId).then(foundOrder => {
		res.send(foundOrder);
	})

};

//[SECTION] UPDATE
//[SECTION] DELETE