// [SECTION] Dependencies and Modules
const exp = require('express');
const res = require('express/lib/response');
const { verify } = require('jsonwebtoken');
const auth = require('../auth.js')
const controller = require('../controllers/users.js');

// [SECTION] Routing Component
const route = exp.Router();

// [SECTION] POST Routes
route.post('/register', controller.registerUser);
route.post('/login', controller.loginUser);

// [SECTION] GET Routes
route.get('/all', auth.verify, auth.verifyAdmin, controller.getAllUsers);
route.get('/:id', auth.verify, auth.verifyAdmin, controller.getUser);

// [SECTION] PUT Routes
route.put('/:id/setAdmin', auth.verify, auth.verifyAdmin, controller.setAdmin);

// [SECTION] DEL Routes
route.delete('/:id', auth.verify, auth.verifyAdmin, controller.deleteUser);


// [SECTION] Export Route System
module.exports = route;