//[SECTION] Dependencies and Modules
const exp = require('express');
const { verify } = require('jsonwebtoken');
const auth = require('../auth.js');
const controller = require('../controllers/products.js');

//[SECTION] Routing Component
const route = exp.Router();

//[SECTION] Post Routes
route.post('/createProduct', auth.verify, auth.verifyAdmin, controller.createProduct);

//[SECTION] Get Routes
route.get('/all', controller.getAllProducts);
route.get('/:id', controller.getProduct);
route.get('/', controller.getAllActive);

//[SECTION] Put Routes
route.put('/:id', auth.verify, auth.verifyAdmin, controller.updateProduct);
route.put('/:id/archive', auth.verify, auth.verifyAdmin, controller.deactivateProduct);

//[SECTION] Del Routes
route.delete('/:id',auth.verify, auth.verifyAdmin, controller.deleteProduct);

// [SECTION] Export Route System
module.exports = route;