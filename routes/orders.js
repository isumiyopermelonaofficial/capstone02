//[SECTION] Dependencies and Modules
const exp = require('express');
const auth = require('../auth.js');
const controller = require('../controllers/orders.js');

//[SECTION] Routing Components
const route = exp.Router();

//[SECTION] POST - Routes
route.post('/createOrder', auth.verify, controller.createOrder);

//[SECTION] GET - Routes
route.get('/getAllOrders', auth.verify, auth.verifyAdmin, controller.getAllOrders);
route.get('/getUserOrder/:id', auth.verify, controller.getUserOrder);

//[SECTION] PUT - Routes
route.put('/addToOrder', auth.verify, controller.addToOrder);
//[SECTION] DELETE - Routes
//[SECTION] Export Route System
module.exports = route;